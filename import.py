import os

from autotweet.learning import DataCollection

DB_URL = os.getenv('DATABASE_URL')

dc = DataCollection(DB_URL)

with open('answers.txt') as fp:
    contents = fp.read().split('\n\n\n')

for content in contents:
    q, a = content.split('\n\n')
    print(f'{q} → {a}')
    dc.add_document(q, a)
