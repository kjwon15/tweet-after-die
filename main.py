from datetime import datetime
import logging
import logging.config
import os
import sys
import time
from urllib.parse import urlencode


from autotweet.daemons import answer_daemon
from autotweet.learning import DataCollection
import tweepy


SOURCE_URL = 'https://github.com/Kjwon15/autotweet/'

CONSUMER_KEY = os.getenv('TWITTER_CONSUMER_KEY')
CONSUMER_SECRET = os.getenv('TWITTER_CONSUMER_SECRET')
ACCESS_KEY = os.getenv('TWITTER_ACCESS_KEY')
ACCESS_SECRET = os.getenv('TWITTER_ACCESS_SECRET')

DB_URL = os.getenv('DATABASE_URL')

logger = logging.getLogger('tad')


def get_fname(fname):
    current_path = os.path.abspath(os.path.dirname(__file__))
    return os.path.join(current_path, fname)


def make_token(key, secret):
    return urlencode({
        'oauth_token': key,
        'oauth_token_secret': secret,
    })


def get_last_tweet(api):
    cursor = tweepy.Cursor(api.user_timeline, user_id=api.me().id)
    for status in cursor.items():
        if status.source_url != SOURCE_URL:
            return status


def announce(api):
    dc = DataCollection(DB_URL)
    count = dc.get_count()
    logger.info(f'Using {count} answers')
    with open(get_fname('msg.txt')) as fp:
        message = fp.read()
        message = message.replace('{count}', str(count))

    logger.info(f'Tweeting: {message}')
    try:
        api.update_status(status=message)
    except Exception as e:
        print(e, file=sys.stderr)


def main():
    auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
    auth.set_access_token(ACCESS_KEY, ACCESS_SECRET)

    logger.info('Starting')
    api = tweepy.API(auth)
    while True:
        status = get_last_tweet(api)
        if not status:
            print('Error!', out=sys.stderr)
            exit(1)

        now = datetime.utcnow()
        delta = now - status.created_at

        logger.info(f'Last tweet at {status.created_at} ({-delta})')
        if delta.days >= 7:
            break
        # Sleep 5 minutes
        time.sleep(5 * 60)

    # Do it!
    # announce(api)

    logger.info('Starting daemon')
    token = make_token(ACCESS_KEY, ACCESS_SECRET)
    answer_daemon(
        token=token,
        db_url=DB_URL,
        streaming=True,
        threshold=1.1,
    )


if __name__ == '__main__':
    logging.config.dictConfig({
        'version': 1,
        'disable_existing_loggers': False,
        'formatters': {
            'brief': {
                'format': '%(asctime)s:%(name)s:%(levelname)s:%(message)s',
                'datefmt': '%Y-%m-%d %H:%M:%S %z',
            }
        },
        'handlers': {
            'console': {
                'class': 'logging.StreamHandler',
                'formatter': 'brief',
                'level': 'DEBUG',
            }
        },
        'loggers': {
            'tad': {
                'handlers': ['console'],
                'level': 'DEBUG',
                'propagate': False,
            },
            'autotweet': {
                'handlers': ['console'],
                'level': 'INFO',
                'propagate': False,
            }
        },
        'root': {
            'level': 'WARNING',
            'handlers': ['console'],
        },
    })
    main()
